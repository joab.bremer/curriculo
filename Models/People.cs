namespace App1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class People
    {

        public People(PeopleDTO peopleDTO)
        {
            this.nome = peopleDTO.nome;
            this.sobreNome = peopleDTO.sNome;
            this.genero = peopleDTO.genero;
            this.dataNasc = peopleDTO.dataNasc;
            this.usuario = peopleDTO.usuario;
            this.senha = peopleDTO.senha;
            this.email = peopleDTO.email;
            this.cpf = peopleDTO.cpf;
            this.idFile = peopleDTO.idFile;
            if (peopleDTO.Id != 0) {
                this.Id = peopleDTO.Id;
            }
            
        }

        public People()
        {
        }

        public People(string nome, string sobreNome, Guid idFile)
        {
            this.nome = nome;
            this.sobreNome = sobreNome;
            this.idFile = idFile;
        }

        public People(string nome, string sobreNome, string genero, DateTime? dataNasc, string usuario, string senha, string email, string cpf, Guid idFile, int id)
        {
            this.nome = nome;
            this.sobreNome = sobreNome;
            this.genero = genero;
            this.dataNasc = dataNasc;
            this.usuario = usuario;
            this.senha = senha;
            this.email = email;
            this.cpf = cpf;
            this.idFile = idFile;
            Id = id;

        }

        [StringLength(255)]
        public string nome { get; set; }

        [StringLength(255)]
        public string sobreNome { get; set; }

        [StringLength(255)]
        public string genero { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dataNasc { get; set; }

        [StringLength(255)]
        public string usuario { get; set; }

        [StringLength(255)]
        public string senha { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(255)]
        public string cpf { get; set; }

        public Guid idFile { get; set; }

        public int Id { get; set; }

        [StringLength(255)]
        public string level { get; set; }
    }
}

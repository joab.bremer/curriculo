﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App1.Models
{
    public class PeopleDTO
    {
        public PeopleDTO()
        {
        }

        public PeopleDTO(string nome, string sNome, Guid idFile)
        {
            this.nome = nome;
            this.sNome = sNome;
            this.idFile = idFile;
        }

        public PeopleDTO(int id, string nome, string sNome, string genero, DateTime dataNasc,
            String usuario, string senha, string email, string cpf)
        {
            this.Id = id;
            this.nome = nome;
            this.sNome = sNome;
            this.genero = genero;
            this.dataNasc = dataNasc;
            this.usuario = usuario;
            this.senha = senha;
            this.email = email;
            this.cpf = cpf;

        }

        public int Id { get; set; }
        public string nome { get; set; }
        public string sNome { get; set; }
        public string genero { get; set; }
        public DateTime? dataNasc { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string email { get; set; }
        public string cpf { get; set; }

        public Guid idFile { get; set; }
        public string level { get; set; }
    }
}
namespace App1.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CurriculoContext : DbContext
    {
        public CurriculoContext()
            : base("name=CurriculoModel")
        {
        }

        public virtual DbSet<Curriculo> Curriculo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

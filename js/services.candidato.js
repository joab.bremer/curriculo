app.service('loginService', ['$cookies',function ($cookies) {

    if($cookies.get('loginData')) {
        this.user =  JSON.parse($cookies.get('loginData'));
        this.userLoged = true;
    }
    else {
        this.userLoged = false;
        this.user={
            name:null,
            level:null
        }
    }
    

    this.path = '/';

    this.refreshUser = function () {
        this.user =  JSON.parse($cookies.get('loginData'));
    }
}]);
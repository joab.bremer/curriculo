// Definindo Rotas
app.config(function($routeProvider, $locationProvider){
	//$locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('');
  $routeProvider
    .when("/login", {
      controller: "login.controller",
      templateUrl:"Views/login.html"
    }).when("/", {
    	controller: "cadastro.candidato.controller",
    	templateUrl:"Views/cadastrar-candidato.html"
    }).when("/candidatos", {
    	controller: "visualizar.candidato.controller",
    	templateUrl:"Views/visualizar-candidato.html"
    }).when('/sobre', {
	  templateUrl : 'Views/about.html',
	  controller  : 'AboutController'
		}).when("/editar-candidato/:idPeople", {
      controller: "editar.candidato.controller",
      templateUrl:"Views/editar-candidato.html"
    }).when("/deletar-candidato/:idPeople", {
      controller: "deletar.candidato.controller",
      template:"<div ng-init='deletar()'></div>"
    }).when("/sair", {
      controller: "login.controller",
      template:"<div ng-init='logout()'></div>"
    })
    .otherwise({redirectTo: '/'});


});

app.controller('editar.candidato.controller', function($scope, $http, $window, $routeParams,$location,loginService){
    console.log("chegou")

    if(loginService.userLoged) {  
        console.log("Logado")
            //chamar função ao iniciar
        $scope.init = function () {

             $http.get('api/People/'+$routeParams.idPeople).
            then(function(response) {
                $scope.candidato = response.data;

                 var e = new Date($scope.candidato.dataNasc);
                    var datestringP = e.getFullYear() + "-" + (e.getMonth() + 1) + "-" + e.getDate();
                    var datestringP = (e.getMonth() + 1)+  "/" +  e.getDate()+ "/" +e.getFullYear() ;
                    $scope.candidato.dataNasc = datestringP;
                
                console.log($scope.candidato);
            });
        }
        //get all
        $scope.getRequest = function () { 
            $http.get("api/People")
                .then(function successCallback(response){
                    console.log("antes dos dados");  
                    $scope.candidatos = response.data;

                    console.log($scope.candidatos);
                }, function errorCallback(response){
                    console.log("Unable to perform get request");
                });
        };

        //download File2
            $scope.download2PDF = function (id) {
            console.log("Entrou na função des download2PDF!");  
            console.log(id)
            $window.open("api/Curriculo/"+id);
        };

//1. Used to list all selected files  
    $scope.files = [];  
  
    //2. a simple model that want to pass to Web API along with selected files  
    //$scope.jsonData = {  
    //    name: $scope.,  
    //    comments: "Multiple upload files"  
    //};  

    //3. listen for the file selected event which is raised from directive  
    $scope.$on("seletedFile", function (event, args) {  
        $scope.$apply(function () {  
            //add the file object to the scope's files collection  
            $scope.files.push(args.file);  
        });  
    });  
  
    //4. Post data and selected files.  
    $scope.save = function () {  

            $scope.Curriculo = {
        Id:$scope.candidato.id,
        nome:$scope.candidato.nome,
        sNome:$scope.candidato.sobreNome,
        genero: $scope.candidato.genero,
        dataNasc:$scope.candidato.dataNasc,
        usuario:$scope.candidato.usuario,
        senha:$scope.candidato.senha,
        email:$scope.candidato.email,
        emailConfirm:$scope.candidato.emailConfirm,
        cpf:$scope.candidato.cpf,
        idFile:$scope.candidato.idFile
        };
        console.log($scope.Curriculo)

        $http({  
            method: 'PUT',  
            url: "api/Curriculo/"+$scope.candidato.id,  
            headers: { 'Content-Type': undefined },  
             
            transformRequest: function (data) {  
                var formData = new FormData();  
                formData.append("model", angular.toJson(data.model));  
                

                for (var i = 0; i < data.files.length; i++) {  
                    formData.append("file" + i, data.files[i]);  
                }  
                return formData;  
            },  
            data: { model: $scope.Curriculo, files: $scope.files }  
        }).  
        then(function (data, status, headers, config) {  
            alert("success!");  
            loginService.path = '/candidatos';
            $location.path('/candidatos');
        }).  
        catch(function (data, status, headers, config) {  
            alert("failed!");  
        });  
    };  
        //Fim do login
      }else{
        console.log("Usuario nao logado")
        alert("Favor realizar login");
        loginService.path = '/login';
        $location.path('/login');

    }
       
    




})

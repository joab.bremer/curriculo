app.directive('uploadFiles', function () {  
    return {  
        scope: true,        //create a new scope  
        link: function (scope, el, attrs) {  

            scope.fileName = 'Choose a file...';

                

            el.bind('change', function (event) {  
                var files = event.target.files;  
                //iterate files since 'multiple' may be specified on the element  

                scope.fileName = files[0].name;
                scope.$emit("seletedFile", { file: files[0] });  
                //for (var i = 0; i < files.length; i++) {  
                //    //emit event upward  
                    
                //}  
            });  
        }  
    };  
});  
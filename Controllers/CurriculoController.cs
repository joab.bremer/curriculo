﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using App1.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace App1.Controllers
{
    public class CurriculoController : ApiController
    {
        private CurriculoContext db = new CurriculoContext();
        private PeopleContext peopleDb = new PeopleContext();

        // GET: api/Curriculo
        public List<CurriculoDTO> GetCurriculo()
        {
            List<CurriculoDTO> curriculos = db.Curriculo.ToList().Select(x => new CurriculoDTO(){Id = x.Id, nome = x.nome}).ToList();
            return curriculos;
        }
        // GET: api/Curriculo/5
        public HttpResponseMessage GetCurriculo(Guid id)
        {
            Curriculo curriculo = db.Curriculo.Find(id);
            //var resp = new HttpResponseMessage(HttpStatusCode.OK);


            //sHttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);
            HttpResponseMessage result = new HttpResponseMessage();
            result.Content = new ByteArrayContent(curriculo.curriculo_file);
            result.Content.Headers.Add("x-filename", curriculo.nome);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = curriculo.nome;
            result.StatusCode = HttpStatusCode.OK;

            var array = curriculo.nome.Split('.');
            string extension = array[1].ToLower();

            
            
            if (extension.Equals("pdf"))
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            }
            if (extension.Equals("docx") || extension.Equals("doc"))
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/ms-word");
            }
            if (extension.Equals("jpg") || extension.Equals("png") || extension.Equals("tiff"))
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/JPEG");
            }



            //result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            return result;



        }


        // PUT: api/Curriculo/5
        //[ResponseType(typeof(void))]

        public async Task<HttpResponseMessage> PutCurriculoAsync(int id)
        {
            Guid idFile = Guid.NewGuid();

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            List<object> lista = new List<object>();
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            PeopleDTO peopleDTO = new PeopleDTO();

            foreach (var requestContents in provider.Contents)
            {


                if (requestContents.Headers.ContentDisposition.Name == "\"model\"")
                {
                    peopleDTO = JsonConvert.DeserializeObject<PeopleDTO>(requestContents.ReadAsStringAsync().Result);


                }
                //else if (requestContents.Headers.ContentDisposition.Name == "order")
                //{
                //    Order q1 = JsonConvert.DeserializeObject<Order>(requestContents.ReadAsStringAsync().Result);
                //     lista.Add(q1);
                // }
            }



            //Salvar arquivo no banco de dados.
            System.Web.HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;


            var result = Request.CreateResponse(HttpStatusCode.BadRequest);
            if (files.Count < 0)
            {
                peopleDTO.idFile = idFile;
                People people = new People(peopleDTO);
                peopleDb.People.Add(people);
                for (int i = 0; i < files.Count; i++)
                {
                    System.Web.HttpPostedFile file = files[i];

                    string fileName = new FileInfo(file.FileName).Name;

                    if (file.ContentLength > 0)
                    {

                        Curriculo curr = new Curriculo();
                        byte[] fileData = null;

                        using (var binaryReader = new BinaryReader(file.InputStream))
                        {
                            fileData = binaryReader.ReadBytes(file.ContentLength);
                        }
                        curr.Id = idFile;
                        curr.nome = fileName;
                        curr.curriculo_file = fileData;
                        db.Curriculo.Add(curr);
                        try
                        {
                            peopleDb.SaveChanges();
                            db.SaveChanges();
                            result = Request.CreateResponse(HttpStatusCode.OK, "success!");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            result = Request.CreateResponse(HttpStatusCode.BadRequest, e);
                        }
                    }
                }


            }
            else
            {
                People people = new People(peopleDTO);
                // peopleDb.People.Add(people);

                peopleDb.Entry(people).State = EntityState.Modified;
                try
                {
                    peopleDb.SaveChanges();
                    result = Request.CreateResponse(HttpStatusCode.OK, "success!");
                }
                catch (Exception e)
                {
                    result = Request.CreateResponse(HttpStatusCode.BadRequest, e);
                }


            }


            return result;
        }
        /* public IHttpActionResult PutCurriculo(int id, Curriculo curriculo)
         {
             if (!ModelState.IsValid)
             {
                 return BadRequest(ModelState);
             }

             if (id.Equals(curriculo.Id))
             {
                 return BadRequest();
             }

             db.Entry(curriculo).State = EntityState.Modified;

             try
             {
                 db.SaveChanges();
             }
             catch (DbUpdateConcurrencyException)
             {
                 if (!CurriculoExists(id))
                 {
                     return NotFound();
                 }
                 else
                 {
                     throw;
                 }
             }

             return StatusCode(HttpStatusCode.NoContent);
         }
         */
        // POST: api/Curriculo
        // [HttpPost]

        public async Task<HttpResponseMessage> Post()
        {

            int uploadCount = 0;
            Guid idFile = Guid.NewGuid();

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            List<object> lista = new List<object>();
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            PeopleDTO peopleDTO = new PeopleDTO();

            foreach (var requestContents in provider.Contents)
            {

                
                if (requestContents.Headers.ContentDisposition.Name == "\"model\"")
                {
                    peopleDTO = JsonConvert.DeserializeObject<PeopleDTO>(requestContents.ReadAsStringAsync().Result);
                    

                }
                //else if (requestContents.Headers.ContentDisposition.Name == "order")
                //{
                //    Order q1 = JsonConvert.DeserializeObject<Order>(requestContents.ReadAsStringAsync().Result);
               //     lista.Add(q1);
               // }
            }

           ///provider.Contents.Headers.ContentDisposition.Name ==""

            //var root = HttpContext.Current.Server.MapPath("~/App_Data/uploads/");
            //Directory.CreateDirectory(root);
            //var provider = new MultipartFormDataStreamProvider();
            //var result = await Request.Content.ReadAsMultipartAsync(provider);
            // var result = await Request.Content.ReadAsMultipartAsync();
           // if (result.FormData["model"] == null)
           // {
           //     throw new HttpResponseException(HttpStatusCode.BadRequest);
           // }

           // string model = result.FormData["model"];
            //JObject jPeople = JObject.Parse(model);

            //DateTime enteredDate = DateTime.Parse(jPeople.GetValue("dataNascimento").ToString());

            //JsonSerializer serializer = new JsonSerializer();
            //PeopleDTO peopleDTO = (PeopleDTO)serializer.Deserialize(new JTokenReader(jPeople), typeof(PeopleDTO));

            //peopleDTO.dataNasc = enteredDate;
            peopleDTO.idFile = idFile;

            People people = new People(peopleDTO);

            peopleDb.People.Add(people);

            //Salvar arquivo no banco de dados.
            System.Web.HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;

            for (int i = 0; i < files.Count; i++)
            {
                System.Web.HttpPostedFile file = files[i];

                string fileName = new FileInfo(file.FileName).Name;

                if (file.ContentLength > 0)
                {
                    
                    Curriculo curr = new Curriculo();
                    byte[] fileData = null;

                    using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(file.ContentLength);
                    }
                    curr.Id = idFile;
                    curr.nome = fileName;
                    curr.curriculo_file = fileData;
                    db.Curriculo.Add(curr);
                    try
                    {
                        peopleDb.SaveChanges();
                        db.SaveChanges();
                        uploadCount++;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        return Request.CreateResponse(HttpStatusCode.BadRequest, e);                        
                    }
                }
            }


            return Request.CreateResponse(HttpStatusCode.OK, "success!");
        }
        /*
    public IHttpActionResult Post()
    {
        int uploadCount = 0;

        //string sPath = System.Web.Hosting.HostingEnvironment.MapPath("/Upload/");
       // var teste = Request.Content.ReadAsAsync

        StreamReader reader = new StreamReader(HttpContext.Current.Request.InputStream);
        string requestFromPost = reader.ReadToEnd();
        var jo = JObject.Parse(requestFromPost);
        string snome = jo.GetValue("sNome").ToString();

        //CurriculoDTO cvDTO = JsonConvert.DeserializeObject<CurriculoDTO>(jo);

        foreach (string key in HttpContext.Current.Request.Form.AllKeys)
        {
            string value = HttpContext.Current.Request.Form[key];
        }

        System.Web.HttpFileCollection files = System.Web.HttpContext.Current.Request.Files;


        for (int i = 0; i < files.Count; i++)
        {
            System.Web.HttpPostedFile file = files[i];

            string fileName = new FileInfo(file.FileName).Name;

            if (file.ContentLength > 0)
            {
                Guid id = Guid.NewGuid();
                Curriculo curr = new Curriculo();
                byte[] fileData = null;

                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }
                curr.Id = id;
                curr.nome = fileName;
                curr.curriculo_file = fileData;
                db.Curriculo.Add(curr);
                try
                {
                    db.SaveChanges();
                    uploadCount++;
                }
                catch (Exception e)
                {
                    return InternalServerError(e);
                    throw;
                }
            }
        }

        if (uploadCount > 0)
        {
            db.SaveChanges();
            return Ok("Success Upload");
        }
        return InternalServerError();

    }

    */


        // DELETE: api/Curriculo/5
        [ResponseType(typeof(Curriculo))]
        public IHttpActionResult DeleteCurriculo(Guid id)
        {
            Curriculo curriculo = db.Curriculo.Find(id);
            if (curriculo == null)
            {
                return NotFound();
            }

            db.Curriculo.Remove(curriculo);
            db.SaveChanges();

            return Ok(curriculo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CurriculoExists(int id)
        {
            return db.Curriculo.Count(e => e.Id.Equals(id) ) > 0;
        }
    }
}
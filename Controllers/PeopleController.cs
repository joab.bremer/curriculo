﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using App1.Models;

namespace App1.Controllers
{
    public class PeopleController : ApiController
    {
        private PeopleContext db = new PeopleContext();

        // GET: api/People
        public List<PeopleDTO> GetPeople()
        {
            List<PeopleDTO> peoples = db.People.ToList().Select(x => new PeopleDTO() {Id =x.Id, nome = x.nome, sNome = x.sobreNome,
                                                                                        idFile = x.idFile, level = x.level}).ToList();
            return peoples;
        }
        //HttpResponseMessage
        // GET: api/People/user/senha
        public IHttpActionResult GetPeople(string usuario, string senha)
        {
            //string usuario = "";
            // People peo = db.People.Find(usuario);
            var peo = db.People.Where(x => x.usuario == usuario).FirstOrDefault();
            return Ok(peo);
        }
        // GET: api/People/5
        [ResponseType(typeof(People))]
        public IHttpActionResult GetPeople(int id)
        {
            People people = db.People.Find(id);
            if (people == null)
            {
                return NotFound();
            }

            return Ok(people);
        }

        // PUT: api/People/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPeople(int id, People people)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != people.Id)
            {
                return BadRequest();
            }

            db.Entry(people).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PeopleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/People
        [ResponseType(typeof(People))]
        public IHttpActionResult PostPeople(People people)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.People.Add(people);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = people.Id }, people);
        }

        // DELETE: api/People/5
        [ResponseType(typeof(People))]
        public IHttpActionResult DeletePeople(int id)
        {
            People people = db.People.Find(id);
            if (people == null)
            {
                return NotFound();
            }

            
            CurriculoController curriculoController = new CurriculoController();
            curriculoController.DeleteCurriculo(people.idFile);
            try{
                db.People.Remove(people);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
            

            return Ok(people);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PeopleExists(int id)
        {
            return db.People.Count(e => e.Id == id) > 0;
        }
    }
}